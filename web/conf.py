# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = "My Cheat Sheets"
copyright = "2022, Kenta Kabashima"
author = "Kenta Kabashima"

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = []

templates_path = ["_templates"]
exclude_patterns = ["Thumbs.db", ".DS_Store"]

language = "ja"

# setting of opengraph
# https://pypi.org/project/sphinxext-opengraph/
extensions += ["sphinxext.opengraph"]
ogp_site_url = "https://cheatsheets.musicscience37.com/"
ogp_site_name = "My Cheat Sheets"
ogp_image = "https://www.musicscience37.com/KIcon128white.png"

# setting of bibtex
# https://sphinxcontrib-bibtex.readthedocs.io/
extensions += ["sphinxcontrib.bibtex"]
bibtex_bibfiles = ["bibliography.bib"]
bibtex_default_style = "plain"

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = "sphinx_orange_book_theme"

html_title = "My Cheat Sheets"

html_static_path = ["_static"]

html_theme_options = {
    "show_prev_next": False,
    "pygment_light_style": "gruvbox-light",
    "pygment_dark_style": "native",
    "repository_url": "https://gitlab.com/MusicScience37/my-cheat-sheets",
    "use_repository_button": True,
}
