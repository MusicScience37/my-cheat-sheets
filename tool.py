#!/usr/bin/env python3
"""このリポジトリ用のヘルパーツール。"""

import os
import shutil
import subprocess
import typing

import click

THIS_DIR = os.path.dirname(os.path.abspath(__file__))


@click.group()
def cli():
    """このリポジトリ用のヘルパーツール。"""


def _write_info(contents: str) -> None:
    """ログを出力する。"""
    click.echo(click.style(contents, bold=True))


# TeX ドキュメントの一覧
ALL_TEX_NAMES = [
    "vector-formulas",
]


def _build_tex(names: typing.List[str] = []) -> None:
    """Tex ドキュメントをビルドする。

    Args:
        names: ビルド対象のドキュメントの一覧。
               デフォルトの空配列の場合は全てビルドする。
    """

    if not names:
        names = ALL_TEX_NAMES

    for name in names:
        _write_info(f"Build {name}.")
        subprocess.run(
            ["latexmk", f"{name}.tex"],
            cwd=os.path.join(THIS_DIR, "tex", name),
            check=True,
        )


def _collect_tex(dir_path: str):
    """TeX ドキュメントを集める。

    Args:
        dir_path (str): ディレクトリパス。
    """

    os.makedirs(dir_path, exist_ok=True)
    for name in ALL_TEX_NAMES:
        shutil.copy(
            os.path.join(THIS_DIR, "tex", name, f"{name}.pdf"),
            dir_path,
        )


def _build_web() -> None:
    """Web 公開用ドキュメントをビルドする。"""

    _collect_tex(os.path.join(THIS_DIR, "build", "html"))
    subprocess.run(
        [
            "sphinx-build",
            "-M",
            "html",
            "web",
            "build",
        ],
        check=True,
        cwd=str(THIS_DIR),
    )


def _collect_all():
    """ビルドされたドキュメントを public ディレクトリに集める。

    GitLab Pages のために使用する。
    """

    public_dir = os.path.join(THIS_DIR, "public")
    shutil.rmtree(public_dir, ignore_errors=True)

    shutil.copytree(
        os.path.join(THIS_DIR, "build", "html"),
        public_dir,
    )


@cli.command()
@click.argument("name", nargs=-1)
def build_tex(name: typing.List[str]) -> None:
    """Tex ドキュメントをビルドする。"""

    _build_tex(name)


@cli.command()
def build_web() -> None:
    """Web 公開用ドキュメントをビルドする。"""

    _build_web()


@cli.command()
def build():
    """全てのドキュメントをビルドする。"""

    _build_web()
    _build_tex()


@cli.command()
def collect():
    """ビルド済みのドキュメントを集める。"""

    _collect_all()


@cli.command()
def web_auto():
    """Web 公開用ドキュメントの自動ビルドを行う。"""

    _collect_tex(os.path.join(THIS_DIR, "build", "auto"))
    subprocess.run(
        [
            "sphinx-autobuild",
            "web",
            "build/auto",
            "--host",
            "0",
            "--port",
            "16347",
        ],
        check=False,
        cwd=str(THIS_DIR),
    )


@cli.command()
def ci_build():
    """CI でビルドの確認を行う。"""

    _build_tex()
    _build_web()


@cli.command()
def ci_publish():
    """CI でドキュメントを公開する。"""

    _build_web()
    _collect_all()


if __name__ == "__main__":
    cli()
